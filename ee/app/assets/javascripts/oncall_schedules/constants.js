export const LENGTH_ENUM = {
  hours: 'HOURS',
  days: 'DAYS',
  weeks: 'WEEKS',
};

export const CHEVRON_SKIPPING_SHADE_ENUM = ['500', '600', '700', '800', '900', '950'];

export const CHEVRON_SKIPPING_PALETTE_ENUM = ['blue', 'orange', 'aqua', 'green', 'magenta'];

export const DAYS_IN_WEEK = 7;
export const HOURS_IN_DAY = 24;

export const PRESET_TYPES = {
  WEEKS: 'WEEKS',
};

export const PRESET_DEFAULTS = {
  WEEKS: {
    TIMEFRAME_LENGTH: 2,
  },
};

export const addRotationModalId = 'addRotationModal';
export const editRotationModalId = 'editRotationModal';
